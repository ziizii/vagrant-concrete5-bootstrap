### Start server ###
Start VM with ```vagrant up``` and then open in browser http://localhost:1234/.

### Database information ###
* Server: ```localhost```
* MySQL Username: ```concrete5```
* MySQL Password: ```,123456```
* Database Name: ```concrete5```

### Basic vagrant commands ###
* ```vagrant up``` - create virtual machine
* ```vagrant ssh``` - connect to VM over SSH

Learn more at https://www.vagrantup.com
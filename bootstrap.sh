#!/bin/sh

sudo yum -y update
sudo yum -y install httpd php php-gd php-mysql mariadb-server mariadb zsh git vim unzip
sudo systemctl enable httpd.service
sudo systemctl enable mariadb.service 
sudo systemctl start httpd.service 
sudo systemctl start mariadb.service 
sudo chsh -s $(which zsh) vagrant
sudo su - vagrant -c 'curl -L http://install.ohmyz.sh | sh; rm -f ~/.zshrc; ln -s /vagrant/zshrc ~/.zshrc'
rm -fr /vagrant/apache/concrete5*
unzip /vagrant/concrete5.7.3.1.zip -d /vagrant/apache
sudo rm -fr /var/www/html
sudo ln -s /vagrant/apache/concrete5.7.3.1/ /var/www/html
sudo mysql -u root < /vagrant/database.sql

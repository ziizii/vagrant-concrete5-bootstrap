CREATE DATABASE `concrete5` CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL ON `concrete5`.* TO `concrete5`@`%` IDENTIFIED BY ',123456';
GRANT ALL ON `concrete5`.* TO `concrete5`@`localhost` IDENTIFIED BY ',123456';
FLUSH PRIVILEGES;
